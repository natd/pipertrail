---
layout: blank
---
<?php

$EmailFrom = "info@lutheranlifevillages.org";
$EmailTo = "info@lutheranlifevillages.org, tneary@lutheranlifevillages.org, jfuller@lutheranlifevillages.org, marissaw@dhdchicago.com";
$Subject = "Piper Trail Event RSVP";
$Name = Trim(stripslashes($_POST['name'])); 
$Phone = Trim(stripslashes($_POST['phone'])); 
$Email = Trim(stripslashes($_POST['email'])); 
$Address = Trim(stripslashes($_POST['address'])); 
$City = Trim(stripslashes($_POST['city']));
$State = Trim(stripslashes($_POST['state'])); 
$Zip = Trim(stripslashes($_POST['zip'])); 
$Event = Trim(stripslashes($_POST['eventDate'])); 
$Attendees = Trim(stripslashes($_POST['attendees']));  


// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

// prepare email body text
$Body = "Piper Trail Event RSVP:";
$Body .= "\n\n";
$Body .= "Event Date: ";
$Body .= $Event;
$Body .= "\n\n";
$Body .= "Name: ";
$Body .= $Name;
$Body .= "\n\n";
$Body .= "Phone: ";
$Body .= $Phone;
$Body .= "\n\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n\n";
$Body .= "Address: ";
$Body .= $Address;
$Body .= "\n";
$Body .= $City;
$Body .= " ";
$Body .= $State;
$Body .= ", ";
$Body .= $Zip;
$Body .= "\n\n";
$Body .= "Number of Attendees: ";
$Body .= $Attendees;
$Body .= "\n\n";
$Body .= "Newsletter signup: ";
$Body .= $Newsletter;
$Body .= "\n";

if($_POST['contact-field'] == '' && $_POST['email-field'] == 'your@email.com') {
	// Not a bot
	// send email 
	$success = mail($EmailTo, $Subject, $Body, "From: <$Email>");
}

// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=/rsvp-thank-you.html\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=/error.html\">";
}
?>
