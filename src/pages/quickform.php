---
layout: blank
---
<?php

$EmailFrom = "info@lutheranlifevillages.org";

$EmailTo = "info@lutheranlifevillages.org, tneary@lutheranlifevillages.org, jfuller@lutheranlifevillages.org, marissaw@dhdchicago.com";
$Subject = "Piper Trail Form Submission";
$Name = Trim(stripslashes($_POST['name'])); 
$Phone = Trim(stripslashes($_POST['phone'])); 
$Email = Trim(stripslashes($_POST['email'])); 
$Message = Trim(stripslashes($_POST['message'])); 
$Interest = Trim(stripslashes($_POST['imInterestedIn'])); 
$Newsletter = Trim(stripslashes($_POST['signUpNewsletter'])); 


// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

// prepare email body text
$Body = "Piper Trail Contact Form Submission:";
$Body .= "\n\n";
$Body .= "Name: ";
$Body .= $Name;
$Body .= "\n\n";
$Body .= "Phone: ";
$Body .= $Phone;
$Body .= "\n\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n\n";
$Body .= "Intrested in: ";
$Body .= $Interest;
$Body .= "\n\n";
$Body .= "Message: ";
$Body .= $Message;
$Body .= "\n\n";
$Body .= "Newsletter signup: ";
$Body .= $Newsletter;
$Body .= "\n";


if($_POST['contact-field'] == '' && $_POST['email-field'] == 'your@email.com') {
  // Not a bot
// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$Email>");


}

// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=/thank-you.html\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=/error.html\">";
}
?>
